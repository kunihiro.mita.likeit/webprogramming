<%@ page language= "java" contentType= "text/html; charset=UTF-8" pageEncoding= "UTF-8" %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"integrity="sha384MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"crossorigin="anonymous">
  <link href="style.css" rel="stylesheet" type="text/css" />

  <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServ" class="navbar-link logout-link">ログアウト</a>
            </li>
  		  </ul>
</head>
<body>


<form class="form-signin" action="RegisterServlet" method="post">
<p class="text-center  font-weight-bold">ユーザ新規登録</p>
<p class="text-left col-4 mx-auto">ユーザId<input type="text" name = "loginId" class="ml-5"style="width:200px;"></p>
<p class="text-left col-4 mx-auto">パスワード<input type="text" name = "password" class="ml-5"style="width:200px;"></p>
<p class="text-left col-6 mx-auto">パスワード(確認)<input type="text"  name = "password2" class="ml-5"style="width:200px;"></p>
<p class="text-left col-4 mx-auto">ユーザ名<input type="text" name = "name" class="ml-5"style="width:200px;"></p>
<p class="text-left col-4 mx-auto">生年月日<input type="date" name = "birthDate" class="ml-5"style="width:200px;"></p>
    <p class="text-center"><input type="submit"  value="登録"></p>
    </form>

    <p class="text-center"><a data-toggle="collapse" href="UserListServ" aria-expanded="false" aria-controls="collapseExample">
    戻る
        </a></p>

        <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
</body>
</html>