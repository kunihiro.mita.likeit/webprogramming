<%@ page language= "java" contentType= "text/html; charset=UTF-8" pageEncoding= "UTF-8" %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>title</title>
    <link rel="stylesheet"
   href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
   integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
   crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />

    <ul class="nav navbar-nav navbar-right">
	<li class="navbar-text">${userInfo.name}さん</li>
	<li class="dropdown"><a href="LogoutServ"
		class="navbar-link logout-link">ログアウト</a></li>
</ul>
</head>
<body>

</div>
    <p class="text-center font-weight-bold">ユーザ情報詳細参照</p>
    <div class="row">
  <div class="col-sm-5">ログインID</div>
  <div class="col-sm-3">${userDetail.loginId}</div>
</div>
<div class="row">
  <div class="col-sm-5">ユーザ名</div>
  <div class="col-sm-3">${userDetail.name}</div>
</div>
    <div class="row">
  <div class="col-sm-5">生年月日</div>
  <div class="col-sm-3">${userDetail.birthDate}</div>
</div>
    <div class="row">
  <div class="col-sm-5">登録日時</div>
  <div class="col-sm-3">${userDetail.createDate}</div>
</div>
    <div class="row">
  <div class="col-sm-5">更新日時</div>
  <div class="col-sm-3">${userDetail.updateDate}</div>
</div>

 <a data-toggle="collapse" href="UserListServ" aria-expanded="false" aria-controls="collapseExample">
    戻る
</a>

</body>
</html>