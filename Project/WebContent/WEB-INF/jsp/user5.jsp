<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
<title>title</title>
<ul class="nav navbar-nav navbar-right">
	<li class="navbar-text">${userInfo.name}さん</li>
	<li class="dropdown"><a href="LogoutServ"
		class="navbar-link logout-link">ログアウト</a></li>
</ul>
</head>
<body>


	<p class="text-center font-weight-bold">ユーザ一削除確認</p>
	<p class="text-left col-3 mx-auto">ログインID:${userDelete.loginId}</p>
	<p class="text-left col-3 mx-auto">を本当に削除してよろしいでしょうか。</p>

	<div class="text-left col-3 mx-auto row">
		<a class="btn btn-primary" href="UserListServ" role="button">キャンセル</a>
		<form class="form-signin" action="UserDeleteServlet" method="post">
			<input type="hidden" name="id" value="${userDelete.id}"> <input
				type="submit" value="OK">
		</form>
	</div>



</body>
</html>