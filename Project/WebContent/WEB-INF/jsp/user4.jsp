<%@ page language= "java" contentType= "text/html; charset=UTF-8" pageEncoding= "UTF-8" %>
<!DOCTYPE html>
<html>
<head>


<meta charset="UTF-8">
 <link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />
<title>title</title>
<ul class="nav navbar-nav navbar-right">
	<li class="navbar-text">${userInfo.name}さん</li>
	<li class="dropdown"><a href="LogoutServ"
		class="navbar-link logout-link">ログアウト</a></li>
</ul>
</head>
<body>


<form class="form-signin" action="UserUpdateServlet" method="post">
<p class="text-center font-weight-bold">ユーザ一情報更新</p>
<div class="row">
  <div class="col-sm-5">ログインID</div>
  <input type="hidden" name="id" value="${userUpdate.id}">
  <div class="col-sm-3">${userUpdate.loginId}</div>
</div>
    <div class="row">
  <div class="col-sm-5">パスワード</div>
  <div class="col-sm-3"><input type="text" name="password" style="width:200px;"></div>
</div>
    <div class="row">
  <div class="col-sm-5">パスワード(確認)</div>
  <div class="col-sm-3"><input type="text" name="password2" style="width:200px;"></div>
</div>
    <div class="row">
  <div class="col-sm-5">ユーザ名</div>
  <div class="col-sm-3"><input type="text"  name="name" style="width:200px;" value="${userUpdate.name}"></div>
</div>
  <div class="col-sm-5">生年月日</div>
  <div class="col-sm-3"><input type="date" name= "date" style="width:200px;" value="${userUpdate.birthDate}"></div>

 <p class="text-left col-4 mx-auto"><input type="submit" value="更新"></p>
 </form>

    <a data-toggle="collapse" href="UserListServ" aria-expanded="false" aria-controls="collapseExample">
    戻る
</a>
 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


</body>
</html>