<%@ page language= "java" contentType= "text/html; charset=UTF-8" pageEncoding= "UTF-8" %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>title</title>
   <link rel="stylesheet"
   href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
   integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
   crossorigin="anonymous">

</head>
<body>
<form class="form-signin" action="LoginServ" method="post">
<p class="text-center font-weight-bold">ログイン画面</p>
<p class="text-left col-4 mx-auto">ログインID<input type="text" name="loginId" class="ml-5"style="width:200px;"></p>
<p class="text-left col-4 mx-auto">パスワード<input type="text" name="password" class="ml-5"style="width:200px;"></p>
<p class="text-center col-4 mx-auto"><input type="submit" value="ログイン"></p>
</form>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

</body>
</html>