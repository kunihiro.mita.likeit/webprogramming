<%@ page language= "java" contentType= "text/html; charset=UTF-8" pageEncoding= "UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
<title>title</title>

<ul class="nav navbar-nav navbar-right">
	<li class="navbar-text">${userInfo.name}さん</li>
	<li class="dropdown"><a href="LogoutServ"
		class="navbar-link logout-link">ログアウト</a></li>
</ul>
</head>
<body>

	<div class="col-2 mx-auto font-weight-bold">
		<p class="col-8 mx-auto">ユーザ一覧</p>
	</div>
	<div class="text-right col-10 mx-auto">
		<a data-toggle="collapse" href="RegisterServlet" aria-expanded="false"
			aria-controls="collapseExample">新規登録 </a>
	</div>

	<form class="form-signin" action="UserListServ" method="post">
	<p class="text-left col-4 mx-auto">
		ログインId<input type="text" name ="loginId" class="ml-5" style="width: 200px;">
	</p>
	<p class="text-left col-4 mx-auto">
		ユーザ名<input type="text" name ="name" class="ml-5" style="width: 200px;">
	</p>
	<p class="text-left col-6 mx-auto">
		ログイン画面 <input type="date" name = "startDate" style="width: 200px;"> ~ <input
			type="date" name = "endDate" style="width: 200px;">
	</p>
	<div class="text-right col-10 mx-auto">
		<input type="submit" value="検索">
	</div>
	</form>

	<div class="text-center col-5 mx-auto">
		<table>
			<tr>
				<th>ログイン</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>

			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<!-- TODO 未実装；ログインボタンの表示制御を行う -->

					<%-- JSPのコメントする時は、これを使う --%>
					<td>
							<!-- userInfoにはログインしたユーザーの情報（name,loginId）が入ってる -->
							<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

							<!-- userInfo(固定) == user(回ってくる）になった時に、実行。c:if の場合は、||、orを使える-->
							<c:if test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>

							<c:if test="${userInfo.loginId == 'admin'}">
							<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
						</c:if></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>