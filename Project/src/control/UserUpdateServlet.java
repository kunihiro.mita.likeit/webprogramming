package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.useDao;
import model.use;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//
		HttpSession session = request.getSession();
		use abc = (use) session.getAttribute("userInfo");
		if (abc == null) {
			response.sendRedirect("LoginServ");
			return;
		}

		String id = request.getParameter("id");

		useDao userDao = new useDao();
		use userUpdate = userDao.allUpdate(id);

		request.setAttribute("userUpdate", userUpdate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user4.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String date = request.getParameter("date");
		String id = request.getParameter("id");

		if (!(password.equals(password2))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			useDao userDao = new useDao();
			use userUpdate = userDao.allUpdate(id);

			request.setAttribute("userUpdate", userUpdate);

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user4.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (name.equals("") || date.equals("") || id.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				useDao userDao = new useDao();
				use userUpdate = userDao.allUpdate(id);

				request.setAttribute("userUpdate", userUpdate);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user4.jsp");
				dispatcher.forward(request, response);
				return;

		} else if (password.equals("") && password2.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			useDao useDao = new useDao();
			useDao.Updat(name, date, id);

			response.sendRedirect("UserListServ");

		} else {

			useDao useDao = new useDao();
			useDao.Update(name, date, password, id);

			response.sendRedirect("UserListServ");

		}
	}
}
