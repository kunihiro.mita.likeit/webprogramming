package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.useDao;
import model.use;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		use abc = (use) session.getAttribute("userInfo");
		if (abc == null) {
			response.sendRedirect("LoginServ");
			return;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user1.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 request.setCharacterEncoding("UTF-8");

			// リクエストパラメータの入力項目を取得
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			String name = request.getParameter("name");
			String birthDate = request.getParameter("birthDate");

			// パスワードとパスワード確認用が違かった時
			if(!(password.equals(password2))) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user1.jsp");
				dispatcher.forward(request, response);
				return;
			}

			// 何か１つでも空欄の物があった時
			if(loginId.equals("") || password.equals("") || password2.equals("") || name.equals("") || birthDate.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user1.jsp");
				dispatcher.forward(request, response);
				return;
			}

			// 既にデータベースに登録されているログインIDを入力した時

			useDao useDao = new useDao();
			use user = useDao.RegisterYet(loginId);
			if(user!=null) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user1.jsp");
				dispatcher.forward(request, response);
				return;
     		}




			useDao.Register(loginId, password, name, birthDate);
			response.sendRedirect("UserListServ");


	}



}
