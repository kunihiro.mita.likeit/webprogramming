package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.use;

public class useDao {
	public use findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new use(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<use> findAll() {
		Connection conn = null;
		List<use> userList = new ArrayList<use>();

		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
			//「’’」がついてなかった→エラーになった
			String sql = "SELECT * FROM user WHERE id!='1'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				use user = new use(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void Register(String loginId, String password, String name, String birthDate) {
		Connection conn = null;

		String result =Seclet(password);

		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user(login_id, password, name, birth_date,create_date,update_date) VALUES (?, ?, ?, ?,NOW(),NOW());";



			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			pStmt.executeUpdate();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public use allDetails(String id){
	Connection conn = null;
	use user=null;
	try {
		// データベースへ接続
		conn = DbManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM user WHERE id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加

			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			user = new use(Id, loginId, name, birthDate, password, createDate, updateDate);

	}catch (SQLException e) {
	e.printStackTrace();
	return null;
	} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	}
	return user;
	}



public use allUpdate(String id){
	Connection conn = null;
	use user=null;
	try {
		// データベースへ接続
		conn = DbManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM user WHERE id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加

			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			user = new use(Id, loginId, name, birthDate, password, createDate, updateDate);

	}catch (SQLException e) {
	e.printStackTrace();
	return null;
	} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	}
	return user;
}

public void Update(String name, String date, String password,String id){
	Connection conn = null;

	String result =Seclet(password);
	try {
		// データベースへ接続
		conn = DbManager.getConnection();


		// SELECT文を準備
		String sql = "UPDATE user SET name= ?, birth_date=?, password=?, update_date = now() WHERE id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, name);
		pStmt.setString(2, date);
		pStmt.setString(3, result);
		pStmt.setString(4, id);
		pStmt.executeUpdate();



	}catch (SQLException e) {
	e.printStackTrace();

	} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	}
}

public void Updat(String name, String date, String id){
	Connection conn = null;

	try {
		// データベースへ接続
		conn = DbManager.getConnection();

		// SELECT文を準備
		String sql = "UPDATE user SET name= ?, birth_date=? ,update_date = now() WHERE id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, name);
		pStmt.setString(2, date);
		pStmt.setString(3, id);
		pStmt.executeUpdate();



	}catch (SQLException e) {
	e.printStackTrace();

	} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	}
}


	public use Delete(String id){
		Connection conn = null;
		use user=null;
		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加

				int Id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				user = new use(Id, loginId, name, birthDate, password, createDate, updateDate);

		}catch (SQLException e) {
		e.printStackTrace();
		return null;
		} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		}
		return user;
	}
	public void DeleteDone(String id){
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
			String sql = "DELETE FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();



		}catch (SQLException e) {
		e.printStackTrace();

		} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		}
	}
	public use RegisterYet(String loginId){
		Connection conn = null;
		use user=null;
		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加

				int Id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				user = new use(Id, loginId1, name, birthDate, password, createDate, updateDate);

		}catch (SQLException e) {
		e.printStackTrace();
		return null;
		} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		}
		return user;

	}

	public String Seclet(String password) {

		String result=null;
		try {
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";
			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			result = DatatypeConverter.printHexBinary(bytes);

		} catch (Exception e) {
			e.printStackTrace();
			}
		return result;
	}
	public List<use> Search(String loginId, String name, String startDate,String endDate ) {
		Connection conn = null;
		List<use> userList = new ArrayList<use>();

		try {
			// データベースへ接続
			conn = DbManager.getConnection();

			// SELECT文を準備
						String sql = "SELECT * FROM user WHERE id!='1'";

						if(!loginId.equals("")) {
							sql += " AND login_id = '" + loginId +"'";
						}

						if(!name.equals("")) {
							sql += " AND name like '%" + name + "%'";
						}

						if(!startDate.equals("")) {
							sql += " AND birth_date >= '" + startDate + "'";
						}

						if(!endDate.equals("")) {
							sql += " AND birth_date <= '" + endDate + "'";
						}

						// SELECTを実行し、結果表を取得
						Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery(sql);



			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId2 = rs.getString("login_id");
				String name2 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				use user = new use(id, loginId2, name2, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}




}






